# Служба SteamCMD CS:Source

### Для установки выполнить:

Копируем в хранилище сервисных скриптов:
```
# cp ./callwatch /etc/init.d/callwatch
```
Делаем его исполняемым:
```
# chmod a+x /etc/init.d/callwatch
```
И делаем прописку в каталогах уровней запуска:
```
# update-rc.d callwatch defaults
```
Активируем и запускаем службу:
```
# systemctl enable callwatch
# systemctl start callwatch
```


###Для информации:
Для выписки (удаления всех симлинков на этот скрипт из всех каталогов уровней запуска) делаем:
````
update-rc.d -f callwatch remove
````
Исходный скрипт /etc/init.d/callwatch при этом не удаляется.